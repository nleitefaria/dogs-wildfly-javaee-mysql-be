package com.mycompany.myapp.dogsbe.service.impl;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import com.mycompany.myapp.dogsbe.dao.BreedDAO;
import com.mycompany.myapp.dogsbe.dao.exception.IllegalOrphanException;
import com.mycompany.myapp.dogsbe.dao.exception.NonexistentEntityException;
import com.mycompany.myapp.dogsbe.dao.impl.BreedDAOImpl;
import com.mycompany.myapp.dogsbe.domain.BreedDTO;
import com.mycompany.myapp.dogsbe.entity.Breed;
import com.mycompany.myapp.dogsbe.service.BreedService;

public class BreedServiceImpl implements BreedService  
{	
	private BreedDAO breedDAO;
	
	public BreedServiceImpl()
	{
		EntityManagerFactory emf = Persistence.createEntityManagerFactory("DogsPU");
		breedDAO = new BreedDAOImpl(emf);
	}
	
	public BreedDTO findBreed(String id) 
	{
		Breed breed = breedDAO.findBreed(Integer.parseInt(id));
        return new BreedDTO(breed.getId(), breed.getName(), breed.getDescr(), breed.getCreationDate(), breed.getLastUpdate());
    }
	
	public List<BreedDTO> findBreedEntities() 
	{
		List<BreedDTO> ret = new ArrayList<BreedDTO>();
        for(Breed breed : breedDAO.findBreedEntities())
        {
        	ret.add(new BreedDTO(breed.getId(), breed.getName(), breed.getDescr(), breed.getCreationDate(), breed.getLastUpdate()));
        }
        return ret;
    }
	
	public void create(BreedDTO breedDTO)
	{	
		breedDAO.create(breedDTO.toBreed());
	}
	
	public void edit(BreedDTO breedDTO) throws IllegalOrphanException, NonexistentEntityException, Exception
	{		
		Breed breed = breedDAO.findBreed(breedDTO.getId());
		
		if(breedDTO.getName()!=null)
			breed.setName(breedDTO.getName());		
		
		if(breedDTO.getDescr()!=null)
			breed.setDescr(breedDTO.getDescr());		
		
		breedDAO.edit(breed);
	}
	
	public void delete(String id) throws NumberFormatException, IllegalOrphanException, NonexistentEntityException 
	{
		breedDAO.destroy(Integer.parseInt(id));
    }

}
