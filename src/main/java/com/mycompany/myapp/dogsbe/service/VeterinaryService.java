package com.mycompany.myapp.dogsbe.service;

import java.util.List;

import com.mycompany.myapp.dogsbe.dao.exception.IllegalOrphanException;
import com.mycompany.myapp.dogsbe.dao.exception.NonexistentEntityException;
import com.mycompany.myapp.dogsbe.domain.VeterinaryDTO;

public interface VeterinaryService
{
	VeterinaryDTO findVeterinary(String id);
	List<VeterinaryDTO> findVeterinaryEntities();
	void create(VeterinaryDTO veterinaryDTO);
	void edit(VeterinaryDTO veterinaryDTO) throws IllegalOrphanException, NonexistentEntityException, Exception;
	void delete(String id) throws NumberFormatException, IllegalOrphanException, NonexistentEntityException ;
}
