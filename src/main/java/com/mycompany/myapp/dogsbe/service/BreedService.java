package com.mycompany.myapp.dogsbe.service;

import java.util.List;

import com.mycompany.myapp.dogsbe.dao.exception.IllegalOrphanException;
import com.mycompany.myapp.dogsbe.dao.exception.NonexistentEntityException;
import com.mycompany.myapp.dogsbe.domain.BreedDTO;

public interface BreedService {
	
	BreedDTO findBreed(String id) ;
	List<BreedDTO> findBreedEntities();
	void create(BreedDTO breedDTO);
	void edit(BreedDTO breedDTO) throws IllegalOrphanException, NonexistentEntityException, Exception;
	void delete(String id) throws NumberFormatException, IllegalOrphanException, NonexistentEntityException;

}
