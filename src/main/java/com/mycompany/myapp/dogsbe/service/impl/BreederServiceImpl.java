package com.mycompany.myapp.dogsbe.service.impl;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import com.mycompany.myapp.dogsbe.dao.BreederDAO;
import com.mycompany.myapp.dogsbe.dao.exception.IllegalOrphanException;
import com.mycompany.myapp.dogsbe.dao.exception.NonexistentEntityException;
import com.mycompany.myapp.dogsbe.dao.impl.BreederDAOImpl;
import com.mycompany.myapp.dogsbe.domain.BreedDTO;
import com.mycompany.myapp.dogsbe.domain.BreederDTO;
import com.mycompany.myapp.dogsbe.entity.Breed;
import com.mycompany.myapp.dogsbe.entity.Breeder;
import com.mycompany.myapp.dogsbe.service.BreederService;

public class BreederServiceImpl implements BreederService 
{
	private BreederDAO breederDAO;
	
	public BreederServiceImpl()
	{
		EntityManagerFactory emf = Persistence.createEntityManagerFactory("DogsPU");
		breederDAO = new BreederDAOImpl(emf);
	}
	
	public void create(BreederDTO breederDTO)
	{	
		breederDAO.create(breederDTO.toBreeder());
	}
	
	public BreederDTO findBreeder(String id) 
	{
		Breeder breeder = breederDAO.findBreeder(Integer.parseInt(id));
		List<BreedDTO> breedDTOList = new ArrayList<BreedDTO>();        			
    	for(Breed breed : breeder.getBreedList())
    	{    		
    		breedDTOList.add(new BreedDTO(breed.getId(), breed.getName(), breed.getDescr(), breed.getCreationDate(), breed.getLastUpdate()));
    	}
        return new BreederDTO(breeder.getId(), breeder.getName(), breeder.getAddress(), breeder.getState(), breeder.getCity(), breeder.getPhone(), breeder.getEmail(), breeder.getCreationDate(), breeder.getLastUpdate(), breedDTOList);
    }
	
	public List<BreederDTO> findBreederEntities() 
	{
		List<BreederDTO> ret = new ArrayList<BreederDTO>();
        for(Breeder breeder : breederDAO.findBreederEntities())
        {       	
        	List<BreedDTO> breedDTOList = new ArrayList<BreedDTO>();        			
        	for(Breed breed : breeder.getBreedList())
        	{    		
        		breedDTOList.add(new BreedDTO(breed.getId(), breed.getName(), breed.getDescr(), breed.getCreationDate(), breed.getLastUpdate()));
        	}
        	
        	ret.add(new BreederDTO(breeder.getId(), breeder.getName(), breeder.getAddress(), breeder.getState(), breeder.getCity(), breeder.getPhone(), breeder.getEmail(), breeder.getCreationDate(), breeder.getLastUpdate(), breedDTOList));
        }
        return ret;
    }
	
	public void edit(BreederDTO breederDTO) throws IllegalOrphanException, NonexistentEntityException, Exception
	{		
		Breeder breeder = breederDAO.findBreeder(breederDTO.getId());
		
		if(breederDTO.getName()!=null)
			breeder.setName(breederDTO.getName());		
		
		breederDAO.edit(breeder);
	}
	
	public void delete(String id) throws NumberFormatException, IllegalOrphanException, NonexistentEntityException 
	{
		breederDAO.destroy(Integer.parseInt(id));
    }

}
