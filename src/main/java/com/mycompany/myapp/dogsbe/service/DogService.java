package com.mycompany.myapp.dogsbe.service;

import java.util.List;

import com.mycompany.myapp.dogsbe.dao.exception.IllegalOrphanException;
import com.mycompany.myapp.dogsbe.dao.exception.NonexistentEntityException;
import com.mycompany.myapp.dogsbe.domain.DogDTO;

public interface DogService
{
	DogDTO findDog(String id);
	List<DogDTO> findDogEntities() ;
	void create(DogDTO dogDTO);
	void edit(DogDTO dogDTO) throws IllegalOrphanException, NonexistentEntityException, Exception;
	void delete(String id) throws NumberFormatException, IllegalOrphanException, NonexistentEntityException;
	
}
