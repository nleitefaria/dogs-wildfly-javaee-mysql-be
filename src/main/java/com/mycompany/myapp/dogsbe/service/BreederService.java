package com.mycompany.myapp.dogsbe.service;

import java.util.List;

import com.mycompany.myapp.dogsbe.dao.exception.IllegalOrphanException;
import com.mycompany.myapp.dogsbe.dao.exception.NonexistentEntityException;
import com.mycompany.myapp.dogsbe.domain.BreederDTO;

public interface BreederService
{
	BreederDTO findBreeder(String id);
	List<BreederDTO> findBreederEntities();
	void create(BreederDTO breederDTO);
	void edit(BreederDTO breederDTO) throws IllegalOrphanException, NonexistentEntityException, Exception;
	void delete(String id) throws NumberFormatException, IllegalOrphanException, NonexistentEntityException;
}
