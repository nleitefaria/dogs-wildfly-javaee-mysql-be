package com.mycompany.myapp.dogsbe.service.impl;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import com.mycompany.myapp.dogsbe.dao.exception.IllegalOrphanException;
import com.mycompany.myapp.dogsbe.dao.exception.NonexistentEntityException;
import com.mycompany.myapp.dogsbe.dao.impl.VeterinaryDAOImpl;
import com.mycompany.myapp.dogsbe.domain.VeterinaryDTO;
import com.mycompany.myapp.dogsbe.entity.Veterinary;
import com.mycompany.myapp.dogsbe.service.VeterinaryService;

public class VeterinaryServiceImpl implements VeterinaryService
{
	private VeterinaryDAOImpl veterinaryDAO;
	
	public VeterinaryServiceImpl()
	{
		EntityManagerFactory emf = Persistence.createEntityManagerFactory("DogsPU");
		veterinaryDAO = new VeterinaryDAOImpl(emf);
	}
	
	public VeterinaryDTO findVeterinary(String id) 
	{
		Veterinary veterinary = veterinaryDAO.findVeterinary(Integer.parseInt(id));
        return new VeterinaryDTO(veterinary.getId(), veterinary.getFirstName(), veterinary.getLastName(), veterinary.getCreationDate(), veterinary.getLastUpdate());
    }
	
	public List<VeterinaryDTO> findVeterinaryEntities() 
	{
		List<VeterinaryDTO> ret = new ArrayList<VeterinaryDTO>();
        for(Veterinary veterinary : veterinaryDAO.findVeterinaryEntities())
        {
        	ret.add(new VeterinaryDTO(veterinary.getId(), veterinary.getFirstName(), veterinary.getLastName(), veterinary.getCreationDate(), veterinary.getLastUpdate()));
        }
        return ret;
    }
	
	public void create(VeterinaryDTO veterinaryDTO)
	{	
		veterinaryDAO.create(veterinaryDTO.toVeterinary());
	}
	
	public void edit(VeterinaryDTO veterinaryDTO) throws IllegalOrphanException, NonexistentEntityException, Exception
	{		
		Veterinary veterinary = veterinaryDAO.findVeterinary(veterinaryDTO.getId());
		
		if(veterinaryDTO.getFirstName()!=null)
			veterinary.setFirstName(veterinaryDTO.getFirstName());		
		if(veterinaryDTO.getLastName()!=null)
			veterinary.setLastName(veterinaryDTO.getLastName());
		
		veterinaryDAO.edit(veterinary);
	}
	
	public void delete(String id) throws NumberFormatException, IllegalOrphanException, NonexistentEntityException 
	{
		veterinaryDAO.destroy(Integer.parseInt(id));
    }

}
