package com.mycompany.myapp.dogsbe.service;

import java.util.List;

import com.mycompany.myapp.dogsbe.domain.RfidDogDTO;

public interface RfidDogService 
{
	RfidDogDTO findRfidDog(String id) ;
	List<RfidDogDTO> findRfidDogEntities();
	void create(RfidDogDTO rfidDogDTO);
}
