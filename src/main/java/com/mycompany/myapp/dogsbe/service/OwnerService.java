package com.mycompany.myapp.dogsbe.service;

import java.util.List;

import com.mycompany.myapp.dogsbe.dao.exception.IllegalOrphanException;
import com.mycompany.myapp.dogsbe.dao.exception.NonexistentEntityException;
import com.mycompany.myapp.dogsbe.domain.OwnerDTO;

public interface OwnerService
{	
	OwnerDTO findOwner(String id);
	List<OwnerDTO> findOwnerEntities();
	void create(OwnerDTO ownerDTO);
	void edit(OwnerDTO ownerDTO) throws IllegalOrphanException, NonexistentEntityException, Exception;
	void delete(String id) throws NumberFormatException, IllegalOrphanException, NonexistentEntityException;
}
