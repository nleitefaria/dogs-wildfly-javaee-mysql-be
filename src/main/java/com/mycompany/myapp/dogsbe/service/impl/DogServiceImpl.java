package com.mycompany.myapp.dogsbe.service.impl;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import com.mycompany.myapp.dogsbe.dao.exception.IllegalOrphanException;
import com.mycompany.myapp.dogsbe.dao.exception.NonexistentEntityException;
import com.mycompany.myapp.dogsbe.dao.impl.DogDAOImpl;
import com.mycompany.myapp.dogsbe.domain.DogDTO;
import com.mycompany.myapp.dogsbe.entity.Dog;
import com.mycompany.myapp.dogsbe.service.DogService;

public class DogServiceImpl implements DogService
{
	
	private DogDAOImpl dogDAO;
	
	public DogServiceImpl()
	{
		EntityManagerFactory emf = Persistence.createEntityManagerFactory("DogsPU");
		dogDAO = new DogDAOImpl(emf);
	}
	
	public DogDTO findDog(String id) 
	{
		Dog dog = dogDAO.findDog(Integer.parseInt(id));
        return new DogDTO(dog.getId(), dog.getName(), dog.getDescr(), dog.getSize(), dog.getBirthDate(), dog.getCreationDate(), dog.getLastUpdate());
    }
	
	public List<DogDTO> findDogEntities() 
	{
		List<DogDTO> ret = new ArrayList<DogDTO>();
        for(Dog dog : dogDAO.findDogEntities())
        {
        	ret.add(new DogDTO(dog.getId(), dog.getName(), dog.getDescr(), dog.getSize(), dog.getBirthDate(), dog.getCreationDate(), dog.getLastUpdate()));
        }
        return ret;
    }
	
	public void create(DogDTO dogDTO)
	{	
		dogDAO.create(dogDTO.toDog());
	}
	
	public void edit(DogDTO dogDTO) throws IllegalOrphanException, NonexistentEntityException, Exception
	{		
		Dog dog = dogDAO.findDog(dogDTO.getId());
		
		if(dogDTO.getName()!=null)
			dog.setName(dogDTO.getName());		
		
		if(dogDTO.getDescr()!=null)
			dog.setDescr(dogDTO.getDescr());		
		
		dogDAO.edit(dog);
	}
	
	public void delete(String id) throws NumberFormatException, IllegalOrphanException, NonexistentEntityException 
	{
		dogDAO.destroy(Integer.parseInt(id));
    }
	
	
	

}
