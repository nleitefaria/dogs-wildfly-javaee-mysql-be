package com.mycompany.myapp.dogsbe.service.impl;

import java.util.ArrayList;
import java.util.List;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import com.mycompany.myapp.dogsbe.dao.RfidDogDAO;
import com.mycompany.myapp.dogsbe.dao.impl.RfidDogDAOImpl;
import com.mycompany.myapp.dogsbe.domain.RfidDogDTO;
import com.mycompany.myapp.dogsbe.entity.RfidDog;
import com.mycompany.myapp.dogsbe.service.RfidDogService;

public class RfidDogServiceImpl implements RfidDogService
{	
	private RfidDogDAO rfidDogDAO;
	
	public RfidDogServiceImpl()
	{
		EntityManagerFactory emf = Persistence.createEntityManagerFactory("DogsPU");
		rfidDogDAO = new RfidDogDAOImpl(emf);
	}
	
	public RfidDogDTO findRfidDog(String id) 
	{
		RfidDog rfidDog = rfidDogDAO.findRfidDog(Integer.parseInt(id));
        return new RfidDogDTO(rfidDog.getDogId(), rfidDog.getBarCode(), rfidDog.getCreationDate(), rfidDog.getLastUpdate());
    }
	
	public List<RfidDogDTO> findRfidDogEntities() 
	{
		List<RfidDogDTO> ret = new ArrayList<RfidDogDTO>();
        for(RfidDog rfidDog : rfidDogDAO.findRfidDogEntities())
        {
        	ret.add(new RfidDogDTO(rfidDog.getDogId(), rfidDog.getBarCode(), rfidDog.getCreationDate(), rfidDog.getLastUpdate()));
        }
        return ret;
    }
	
	public void create(RfidDogDTO rfidDogDTO)
	{	
		rfidDogDAO.create(rfidDogDTO.toRfidDog());
	}
}
