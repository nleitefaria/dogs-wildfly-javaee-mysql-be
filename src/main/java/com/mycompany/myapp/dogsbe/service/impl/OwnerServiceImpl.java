package com.mycompany.myapp.dogsbe.service.impl;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import com.mycompany.myapp.dogsbe.dao.exception.IllegalOrphanException;
import com.mycompany.myapp.dogsbe.dao.exception.NonexistentEntityException;
import com.mycompany.myapp.dogsbe.dao.impl.OwnerDAOImpl;
import com.mycompany.myapp.dogsbe.domain.OwnerContactDTO;
import com.mycompany.myapp.dogsbe.domain.OwnerDTO;
import com.mycompany.myapp.dogsbe.entity.Owner;
import com.mycompany.myapp.dogsbe.service.OwnerService;

public class OwnerServiceImpl implements OwnerService
{
	private OwnerDAOImpl ownerDAO;
	
	public OwnerServiceImpl()
	{
		EntityManagerFactory emf = Persistence.createEntityManagerFactory("DogsPU");
		ownerDAO = new OwnerDAOImpl(emf);
	}
	
	public OwnerDTO findOwner(String id) 
	{
		Owner owner = ownerDAO.findOwner(Integer.parseInt(id));
        return new OwnerDTO(owner.getId(), owner.getFirstName(), owner.getLastName(), owner.getCreationDate(), owner.getLastUpdate(), new OwnerContactDTO(owner.getOwnerContact().getAddress(), owner.getOwnerContact().getCity(), owner.getOwnerContact().getCountry(), owner.getOwnerContact().getPhone(), owner.getOwnerContact().getMobile(),owner.getOwnerContact().getCreationDate(), owner.getOwnerContact().getLastUpdate()));
    }
	
	public List<OwnerDTO> findOwnerEntities() 
	{
		List<OwnerDTO> ret = new ArrayList<OwnerDTO>();
        
		for(Owner owner : ownerDAO.findOwnerEntities())
		{
			ret.add(new OwnerDTO(owner.getId(), owner.getFirstName(), owner.getLastName(), owner.getCreationDate(), owner.getLastUpdate(), new OwnerContactDTO(owner.getOwnerContact().getAddress(), owner.getOwnerContact().getCity(), owner.getOwnerContact().getCountry(), owner.getOwnerContact().getPhone(), owner.getOwnerContact().getMobile(),owner.getOwnerContact().getCreationDate(), owner.getOwnerContact().getLastUpdate())));
		}
	
        return ret;
    }
	
	public void create(OwnerDTO ownerDTO)
	{	
		ownerDAO.create(ownerDTO.toOwner());
	}
	
	public void edit(OwnerDTO ownerDTO) throws IllegalOrphanException, NonexistentEntityException, Exception
	{		
		Owner owner = ownerDAO.findOwner(ownerDTO.getId());
		
		if(ownerDTO.getFirstName()!=null)
			owner.setFirstName(ownerDTO.getFirstName());		
		if(ownerDTO.getLastName()!=null)
			owner.setLastName(ownerDTO.getLastName());
		
		ownerDAO.edit(owner);
	}

	public void delete(String id) throws NumberFormatException, IllegalOrphanException, NonexistentEntityException 
	{
		ownerDAO.destroy(Integer.parseInt(id));
    }
}
