package com.mycompany.myapp.dogsbe.domain;

import java.util.Date;
import com.mycompany.myapp.dogsbe.entity.Breed;

public class BreedDTO
{
	private Integer id;
	private String name;
	private String descr;
	private Date creationDate;
	private Date lastUpdate;

	public BreedDTO() 
	{
	}

	public BreedDTO(Integer id, String name, String descr, Date creationDate, Date lastUpdate) 
	{
		this.id = id;
		this.name = name;
		this.descr = descr;
		this.creationDate = creationDate;
		this.lastUpdate = lastUpdate;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescr() {
		return descr;
	}

	public void setDescr(String descr) {
		this.descr = descr;
	}

	public Date getCreationDate() {
		return creationDate;
	}

	public void setCreationDate(Date creationDate) {
		this.creationDate = creationDate;
	}

	public Date getLastUpdate() {
		return lastUpdate;
	}

	public void setLastUpdate(Date lastUpdate) {
		this.lastUpdate = lastUpdate;
	}

	public Breed toBreed() 
	{
		Breed breed = new Breed();
		breed.setName(getName());
		breed.setDescr(getDescr());
		breed.setCreationDate(new Date());
		breed.setLastUpdate(new Date());
		return breed;
	}
}
