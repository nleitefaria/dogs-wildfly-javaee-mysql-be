package com.mycompany.myapp.dogsbe.domain;

import java.util.Date;

import com.mycompany.myapp.dogsbe.entity.Dog;




public class DogDTO 
{
	private Integer id;  
    private String name;   
    private String descr; 
    private String size;  
    private Date birthDate;  
    private Date creationDate;  
    private Date lastUpdate;
    
	public DogDTO() {
	}

	public DogDTO(Date birthDate, Date creationDate, Date lastUpdate) {
		this.birthDate = birthDate;
		this.creationDate = creationDate;
		this.lastUpdate = lastUpdate;
	}

	public DogDTO(Integer id, String name, String descr, String size, Date birthDate, Date creationDate, Date lastUpdate) {
		this.id = id;
		this.name = name;
		this.descr = descr;
		this.size = size;
		this.birthDate = birthDate;
		this.creationDate = creationDate;
		this.lastUpdate = lastUpdate;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescr() {
		return descr;
	}

	public void setDescr(String descr) {
		this.descr = descr;
	}

	public String getSize() {
		return size;
	}

	public void setSize(String size) {
		this.size = size;
	}

	public Date getBirthDate() {
		return birthDate;
	}

	public void setBirthDate(Date birthDate) {
		this.birthDate = birthDate;
	}

	public Date getCreationDate() {
		return creationDate;
	}


	public void setCreationDate(Date creationDate) {
		this.creationDate = creationDate;
	}

	public Date getLastUpdate() {
		return lastUpdate;
	}

	public void setLastUpdate(Date lastUpdate) {
		this.lastUpdate = lastUpdate;
	}
	
	public Dog toDog()
	{
		Dog dog = new Dog();
		dog.setBirthDate(new Date());
		dog.setCreationDate(new Date());
		dog.setLastUpdate(new Date());
		return dog;	
	}
	
}
