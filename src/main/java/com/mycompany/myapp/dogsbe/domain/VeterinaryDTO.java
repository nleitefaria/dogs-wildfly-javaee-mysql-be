package com.mycompany.myapp.dogsbe.domain;

import java.util.Date;

import com.mycompany.myapp.dogsbe.entity.Veterinary;

public class VeterinaryDTO 
{
	private Integer id;
	private String firstName;
	private String lastName;
	private Date creationDate;
	private Date lastUpdate;

	public VeterinaryDTO() {
	}

	public VeterinaryDTO(Integer id, String firstName, String lastName, Date creationDate, Date lastUpdate) 
	{
		this.id = id;
		this.firstName = firstName;
		this.lastName = lastName;
		this.creationDate = creationDate;
		this.lastUpdate = lastUpdate;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public Date getCreationDate() {
		return creationDate;
	}

	public void setCreationDate(Date creationDate) {
		this.creationDate = creationDate;
	}

	public Date getLastUpdate() {
		return lastUpdate;
	}

	public void setLastUpdate(Date lastUpdate) {
		this.lastUpdate = lastUpdate;
	}
	
	public Veterinary toVeterinary() 
	{
		Veterinary veterinary = new Veterinary();
		veterinary.setFirstName(getFirstName());
		veterinary.setLastName(getLastName());
		veterinary.setCreationDate(new Date());
		veterinary.setLastUpdate(new Date());
		return veterinary;
	}

}
