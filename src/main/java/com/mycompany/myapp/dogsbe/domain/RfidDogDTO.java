package com.mycompany.myapp.dogsbe.domain;

import java.util.Date;

import com.mycompany.myapp.dogsbe.entity.RfidDog;

public class RfidDogDTO 
{
	private Integer dogId;
	private String barCode;
	private String notes;
	private String isoCompliant;
	private Date creationDate;
	private Date lastUpdate;
	
	public RfidDogDTO() {
		
	}

	public RfidDogDTO(Integer dogId, String barCode, Date creationDate, Date lastUpdate) {
		this.dogId = dogId;
		this.barCode = barCode;
		this.creationDate = creationDate;
		this.lastUpdate = lastUpdate;
	}

	public Integer getDogId() {
		return dogId;
	}

	public void setDogId(Integer dogId) {
		this.dogId = dogId;
	}

	public String getBarCode() {
		return barCode;
	}

	public void setBarCode(String barCode) {
		this.barCode = barCode;
	}

	public Date getCreationDate() {
		return creationDate;
	}

	public void setCreationDate(Date creationDate) {
		this.creationDate = creationDate;
	}

	public Date getLastUpdate() {
		return lastUpdate;
	}

	public void setLastUpdate(Date lastUpdate) {
		this.lastUpdate = lastUpdate;
	}
	
	public String getNotes() {
		return notes;
	}

	public void setNotes(String notes) {
		this.notes = notes;
	}

	public String getIsoCompliant() {
		return isoCompliant;
	}

	public void setIsoCompliant(String isoCompliant) {
		this.isoCompliant = isoCompliant;
	}

	public RfidDog toRfidDog() 
	{
		RfidDog rfidDog = new RfidDog();
		rfidDog.setDogId(getDogId());
		rfidDog.setBarCode(getBarCode());
		rfidDog.setIsoCompliant(getIsoCompliant());
		rfidDog.setNotes(getNotes());	
		return rfidDog;
	}
}
