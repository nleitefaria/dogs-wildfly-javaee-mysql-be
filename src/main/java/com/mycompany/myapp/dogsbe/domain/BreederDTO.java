package com.mycompany.myapp.dogsbe.domain;

import java.util.Date;
import java.util.List;

import com.mycompany.myapp.dogsbe.entity.Breeder;

public class BreederDTO 
{	
	private Integer id; 
    private String name;  
    private String address;
    private String state;  
    private String city;  
    private String phone;  
    private String email;    
    private Date creationDate;   
    private Date lastUpdate;  
    private List<BreedDTO> breedList;
    
	public BreederDTO() {	
	}

	public BreederDTO(Integer id, String name, String address, String state, String city, String phone, String email, Date creationDate, Date lastUpdate, List<BreedDTO> breedList) 
	{	
		this.id = id;
		this.name = name;
		this.address = address;
		this.state = state;
		this.city = city;
		this.phone = phone;
		this.email = email;
		this.creationDate = creationDate;
		this.lastUpdate = lastUpdate;
		this.breedList = breedList;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public Date getCreationDate() {
		return creationDate;
	}

	public void setCreationDate(Date creationDate) {
		this.creationDate = creationDate;
	}

	public Date getLastUpdate() {
		return lastUpdate;
	}

	public void setLastUpdate(Date lastUpdate) {
		this.lastUpdate = lastUpdate;
	}

	public List<BreedDTO> getBreedList() {
		return breedList;
	}

	public void setBreedList(List<BreedDTO> breedList) {
		this.breedList = breedList;
	}
	
	public Breeder toBreeder() 
	{
		Breeder breeder = new Breeder();
		breeder.setName(getName());
		breeder.setAddress(getAddress());
		breeder.setState(getState());
		breeder.setCity(getCity());
		breeder.setPhone(getPhone());
		breeder.setEmail(getEmail());
		breeder.setCreationDate(new Date());
		breeder.setLastUpdate(new Date());	
		return breeder;
	}
}
