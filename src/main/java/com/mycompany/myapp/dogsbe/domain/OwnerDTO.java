package com.mycompany.myapp.dogsbe.domain;

import java.util.Date;

import com.mycompany.myapp.dogsbe.entity.Owner;

public class OwnerDTO 
{
	private Integer id;
	private String firstName;
	private String lastName;
	private Date creationDate;
	private Date lastUpdate;
	private OwnerContactDTO ownerContact;

	public OwnerDTO() {
	}

	public OwnerDTO(Date creationDate, Date lastUpdate)
	{
		this.creationDate = creationDate;
		this.lastUpdate = lastUpdate;
	}

	public OwnerDTO(Integer id, String firstName, String lastName, Date creationDate, Date lastUpdate, OwnerContactDTO ownerContact)
	{	
		this.id = id;
		this.firstName = firstName;
		this.lastName = lastName;
		this.creationDate = creationDate;
		this.lastUpdate = lastUpdate;
		this.ownerContact = ownerContact;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public Date getCreationDate() {
		return creationDate;
	}

	public void setCreationDate(Date creationDate) {
		this.creationDate = creationDate;
	}

	public Date getLastUpdate() {
		return lastUpdate;
	}

	public void setLastUpdate(Date lastUpdate) {
		this.lastUpdate = lastUpdate;
	}

	public OwnerContactDTO getOwnerContactDTO() {
		return ownerContact;
	}

	public void setOwnerContactDTO(OwnerContactDTO ownerContact) {
		this.ownerContact = ownerContact;
	}
	
	public Owner toOwner() 
	{
		Owner owner = new Owner();
		
		if(getId()!=null)
			owner.setId(getId());
		
		owner.setFirstName(getFirstName());
		owner.setLastName(getLastName());
		owner.setCreationDate(new Date());
		owner.setLastUpdate(new Date());
		return owner;
	}
}
