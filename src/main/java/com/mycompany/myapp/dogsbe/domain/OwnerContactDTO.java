package com.mycompany.myapp.dogsbe.domain;

import java.util.Date;

public class OwnerContactDTO 
{
	private String address;  
    private String city; 
    private String country;
    private String phone;
    private String mobile;
    private Date creationDate;  
    private Date lastUpdate;
    
	public OwnerContactDTO()
	{
	}
	
	public OwnerContactDTO(String address, String city, String country, String phone, String mobile, Date creationDate, Date lastUpdate)
	{		
		this.address = address;
		this.city = city;
		this.country = country;
		this.phone = phone;
		this.mobile = mobile;
		this.creationDate = creationDate;
		this.lastUpdate = lastUpdate;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getMobile() {
		return mobile;
	}

	public void setMobile(String mobile) {
		this.mobile = mobile;
	}

	public Date getCreationDate() {
		return creationDate;
	}

	public void setCreationDate(Date creationDate) {
		this.creationDate = creationDate;
	}

	public Date getLastUpdate() {
		return lastUpdate;
	}

	public void setLastUpdate(Date lastUpdate) {
		this.lastUpdate = lastUpdate;
	}
	
	
    
    

}
