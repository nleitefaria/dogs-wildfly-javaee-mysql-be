/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.myapp.dogsbe.entity;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 *
 * @author nleit_000
 */
@Entity
@Table(name = "rfid_dog")
@NamedQueries({
    @NamedQuery(name = "RfidDog.findAll", query = "SELECT r FROM RfidDog r"),
    @NamedQuery(name = "RfidDog.findByDogId", query = "SELECT r FROM RfidDog r WHERE r.dogId = :dogId"),
    @NamedQuery(name = "RfidDog.findByBarCode", query = "SELECT r FROM RfidDog r WHERE r.barCode = :barCode"),
    @NamedQuery(name = "RfidDog.findByIsoCompliant", query = "SELECT r FROM RfidDog r WHERE r.isoCompliant = :isoCompliant"),
    @NamedQuery(name = "RfidDog.findByCreationDate", query = "SELECT r FROM RfidDog r WHERE r.creationDate = :creationDate"),
    @NamedQuery(name = "RfidDog.findByLastUpdate", query = "SELECT r FROM RfidDog r WHERE r.lastUpdate = :lastUpdate")})
public class RfidDog implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "dog_id")
    private Integer dogId;
    @Basic(optional = false)
    @Column(name = "bar_code")
    private String barCode;
    @Lob
    @Column(name = "notes")
    private String notes;
    @Column(name = "iso_compliant")
    private String isoCompliant;
    @Basic(optional = false)
    @Column(name = "creation_date")
    @Temporal(TemporalType.TIMESTAMP)
    private Date creationDate;
    @Basic(optional = false)
    @Column(name = "last_update")
    @Temporal(TemporalType.TIMESTAMP)
    private Date lastUpdate;
    @JoinColumn(name = "dog_id", referencedColumnName = "id", insertable = false, updatable = false)
    @OneToOne(optional = false)
    private Dog dog;

    public RfidDog() {
    }

    public RfidDog(Integer dogId) {
        this.dogId = dogId;
    }

    public RfidDog(Integer dogId, String barCode, Date creationDate, Date lastUpdate) {
        this.dogId = dogId;
        this.barCode = barCode;
        this.creationDate = creationDate;
        this.lastUpdate = lastUpdate;
    }

    public Integer getDogId() {
        return dogId;
    }

    public void setDogId(Integer dogId) {
        this.dogId = dogId;
    }

    public String getBarCode() {
        return barCode;
    }

    public void setBarCode(String barCode) {
        this.barCode = barCode;
    }

    public String getNotes() {
        return notes;
    }

    public void setNotes(String notes) {
        this.notes = notes;
    }

    public String getIsoCompliant() {
        return isoCompliant;
    }

    public void setIsoCompliant(String isoCompliant) {
        this.isoCompliant = isoCompliant;
    }

    public Date getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(Date creationDate) {
        this.creationDate = creationDate;
    }

    public Date getLastUpdate() {
        return lastUpdate;
    }

    public void setLastUpdate(Date lastUpdate) {
        this.lastUpdate = lastUpdate;
    }

    public Dog getDog() {
        return dog;
    }

    public void setDog(Dog dog) {
        this.dog = dog;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (dogId != null ? dogId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof RfidDog)) {
            return false;
        }
        RfidDog other = (RfidDog) object;
        if ((this.dogId == null && other.dogId != null) || (this.dogId != null && !this.dogId.equals(other.dogId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.mycompany.myapp.RfidDog[ dogId=" + dogId + " ]";
    }
    
}
