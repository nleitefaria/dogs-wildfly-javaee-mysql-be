/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.myapp.dogsbe.entity;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 *
 * @author nleit_000
 */
@Entity
@Table(name = "veterinary_contact")
@NamedQueries({
    @NamedQuery(name = "VeterinaryContact.findAll", query = "SELECT v FROM VeterinaryContact v"),
    @NamedQuery(name = "VeterinaryContact.findByVeterinaryId", query = "SELECT v FROM VeterinaryContact v WHERE v.veterinaryId = :veterinaryId"),
    @NamedQuery(name = "VeterinaryContact.findByAddress", query = "SELECT v FROM VeterinaryContact v WHERE v.address = :address"),
    @NamedQuery(name = "VeterinaryContact.findByCity", query = "SELECT v FROM VeterinaryContact v WHERE v.city = :city"),
    @NamedQuery(name = "VeterinaryContact.findByCountry", query = "SELECT v FROM VeterinaryContact v WHERE v.country = :country"),
    @NamedQuery(name = "VeterinaryContact.findByPhone", query = "SELECT v FROM VeterinaryContact v WHERE v.phone = :phone"),
    @NamedQuery(name = "VeterinaryContact.findByMobile", query = "SELECT v FROM VeterinaryContact v WHERE v.mobile = :mobile"),
    @NamedQuery(name = "VeterinaryContact.findByCreationDate", query = "SELECT v FROM VeterinaryContact v WHERE v.creationDate = :creationDate"),
    @NamedQuery(name = "VeterinaryContact.findByLastUpdate", query = "SELECT v FROM VeterinaryContact v WHERE v.lastUpdate = :lastUpdate")})
public class VeterinaryContact implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "veterinary_id")
    private Integer veterinaryId;
    @Basic(optional = false)
    @Column(name = "address")
    private String address;
    @Basic(optional = false)
    @Column(name = "city")
    private String city;
    @Basic(optional = false)
    @Column(name = "country")
    private String country;
    @Column(name = "phone")
    private String phone;
    @Column(name = "mobile")
    private String mobile;
    @Basic(optional = false)
    @Column(name = "creation_date")
    @Temporal(TemporalType.TIMESTAMP)
    private Date creationDate;
    @Basic(optional = false)
    @Column(name = "last_update")
    @Temporal(TemporalType.TIMESTAMP)
    private Date lastUpdate;
    @JoinColumn(name = "veterinary_id", referencedColumnName = "id", insertable = false, updatable = false)
    @OneToOne(optional = false)
    private Veterinary veterinary;

    public VeterinaryContact() {
    }

    public VeterinaryContact(Integer veterinaryId) {
        this.veterinaryId = veterinaryId;
    }

    public VeterinaryContact(Integer veterinaryId, String address, String city, String country, Date creationDate, Date lastUpdate) {
        this.veterinaryId = veterinaryId;
        this.address = address;
        this.city = city;
        this.country = country;
        this.creationDate = creationDate;
        this.lastUpdate = lastUpdate;
    }

    public Integer getVeterinaryId() {
        return veterinaryId;
    }

    public void setVeterinaryId(Integer veterinaryId) {
        this.veterinaryId = veterinaryId;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public Date getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(Date creationDate) {
        this.creationDate = creationDate;
    }

    public Date getLastUpdate() {
        return lastUpdate;
    }

    public void setLastUpdate(Date lastUpdate) {
        this.lastUpdate = lastUpdate;
    }

    public Veterinary getVeterinary() {
        return veterinary;
    }

    public void setVeterinary(Veterinary veterinary) {
        this.veterinary = veterinary;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (veterinaryId != null ? veterinaryId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof VeterinaryContact)) {
            return false;
        }
        VeterinaryContact other = (VeterinaryContact) object;
        if ((this.veterinaryId == null && other.veterinaryId != null) || (this.veterinaryId != null && !this.veterinaryId.equals(other.veterinaryId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.mycompany.myapp.VeterinaryContact[ veterinaryId=" + veterinaryId + " ]";
    }
    
}
