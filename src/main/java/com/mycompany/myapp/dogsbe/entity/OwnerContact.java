/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.myapp.dogsbe.entity;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 *
 * @author nleit_000
 */
@Entity
@Table(name = "owner_contact")
@NamedQueries({
    @NamedQuery(name = "OwnerContact.findAll", query = "SELECT o FROM OwnerContact o"),
    @NamedQuery(name = "OwnerContact.findByOwnerId", query = "SELECT o FROM OwnerContact o WHERE o.ownerId = :ownerId"),
    @NamedQuery(name = "OwnerContact.findByAddress", query = "SELECT o FROM OwnerContact o WHERE o.address = :address"),
    @NamedQuery(name = "OwnerContact.findByCity", query = "SELECT o FROM OwnerContact o WHERE o.city = :city"),
    @NamedQuery(name = "OwnerContact.findByCountry", query = "SELECT o FROM OwnerContact o WHERE o.country = :country"),
    @NamedQuery(name = "OwnerContact.findByPhone", query = "SELECT o FROM OwnerContact o WHERE o.phone = :phone"),
    @NamedQuery(name = "OwnerContact.findByMobile", query = "SELECT o FROM OwnerContact o WHERE o.mobile = :mobile"),
    @NamedQuery(name = "OwnerContact.findByCreationDate", query = "SELECT o FROM OwnerContact o WHERE o.creationDate = :creationDate"),
    @NamedQuery(name = "OwnerContact.findByLastUpdate", query = "SELECT o FROM OwnerContact o WHERE o.lastUpdate = :lastUpdate")})
public class OwnerContact implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "owner_id")
    private Integer ownerId;
    @Basic(optional = false)
    @Column(name = "address")
    private String address;
    @Basic(optional = false)
    @Column(name = "city")
    private String city;
    @Basic(optional = false)
    @Column(name = "country")
    private String country;
    @Column(name = "phone")
    private String phone;
    @Column(name = "mobile")
    private String mobile;
    @Basic(optional = false)
    @Column(name = "creation_date")
    @Temporal(TemporalType.TIMESTAMP)
    private Date creationDate;
    @Basic(optional = false)
    @Column(name = "last_update")
    @Temporal(TemporalType.TIMESTAMP)
    private Date lastUpdate;
    @JoinColumn(name = "owner_id", referencedColumnName = "id", insertable = false, updatable = false)
    @OneToOne(optional = false)
    private Owner owner;

    public OwnerContact() {
    }

    public OwnerContact(Integer ownerId) {
        this.ownerId = ownerId;
    }

    public OwnerContact(Integer ownerId, String address, String city, String country, Date creationDate, Date lastUpdate) {
        this.ownerId = ownerId;
        this.address = address;
        this.city = city;
        this.country = country;
        this.creationDate = creationDate;
        this.lastUpdate = lastUpdate;
    }

    public Integer getOwnerId() {
        return ownerId;
    }

    public void setOwnerId(Integer ownerId) {
        this.ownerId = ownerId;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public Date getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(Date creationDate) {
        this.creationDate = creationDate;
    }

    public Date getLastUpdate() {
        return lastUpdate;
    }

    public void setLastUpdate(Date lastUpdate) {
        this.lastUpdate = lastUpdate;
    }

    public Owner getOwner() {
        return owner;
    }

    public void setOwner(Owner owner) {
        this.owner = owner;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (ownerId != null ? ownerId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof OwnerContact)) {
            return false;
        }
        OwnerContact other = (OwnerContact) object;
        if ((this.ownerId == null && other.ownerId != null) || (this.ownerId != null && !this.ownerId.equals(other.ownerId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.mycompany.myapp.OwnerContact[ ownerId=" + ownerId + " ]";
    }
    
}
