/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.myapp.dogsbe.entity;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.ManyToMany;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 *
 * @author nleit_000
 */
@Entity
@Table(name = "breeder")
@NamedQueries({
    @NamedQuery(name = "Breeder.findAll", query = "SELECT b FROM Breeder b"),
    @NamedQuery(name = "Breeder.findById", query = "SELECT b FROM Breeder b WHERE b.id = :id"),
    @NamedQuery(name = "Breeder.findByName", query = "SELECT b FROM Breeder b WHERE b.name = :name"),
    @NamedQuery(name = "Breeder.findByState", query = "SELECT b FROM Breeder b WHERE b.state = :state"),
    @NamedQuery(name = "Breeder.findByCity", query = "SELECT b FROM Breeder b WHERE b.city = :city"),
    @NamedQuery(name = "Breeder.findByPhone", query = "SELECT b FROM Breeder b WHERE b.phone = :phone"),
    @NamedQuery(name = "Breeder.findByEmail", query = "SELECT b FROM Breeder b WHERE b.email = :email"),
    @NamedQuery(name = "Breeder.findByCreationDate", query = "SELECT b FROM Breeder b WHERE b.creationDate = :creationDate"),
    @NamedQuery(name = "Breeder.findByLastUpdate", query = "SELECT b FROM Breeder b WHERE b.lastUpdate = :lastUpdate")})
public class Breeder implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;
    @Column(name = "name")
    private String name;
    @Lob
    @Column(name = "address")
    private String address;
    @Column(name = "state")
    private String state;
    @Column(name = "city")
    private String city;
    @Column(name = "phone")
    private String phone;
    @Column(name = "email")
    private String email;
    @Basic(optional = false)
    @Column(name = "creation_date")
    @Temporal(TemporalType.TIMESTAMP)
    private Date creationDate;
    @Basic(optional = false)
    @Column(name = "last_update")
    @Temporal(TemporalType.TIMESTAMP)
    private Date lastUpdate;
    @ManyToMany(mappedBy = "breederList")
    private List<Breed> breedList;

    public Breeder() {
    }

    public Breeder(Integer id) {
        this.id = id;
    }

    public Breeder(Integer id, Date creationDate, Date lastUpdate) {
        this.id = id;
        this.creationDate = creationDate;
        this.lastUpdate = lastUpdate;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Date getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(Date creationDate) {
        this.creationDate = creationDate;
    }

    public Date getLastUpdate() {
        return lastUpdate;
    }

    public void setLastUpdate(Date lastUpdate) {
        this.lastUpdate = lastUpdate;
    }

    public List<Breed> getBreedList() {
        return breedList;
    }

    public void setBreedList(List<Breed> breedList) {
        this.breedList = breedList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Breeder)) {
            return false;
        }
        Breeder other = (Breeder) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.mycompany.myapp.Breeder[ id=" + id + " ]";
    }
    
}
