/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.myapp.dogsbe.entity;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.Lob;
import javax.persistence.ManyToMany;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 *
 * @author nleit_000
 */
@Entity
@Table(name = "breed")
@NamedQueries({
    @NamedQuery(name = "Breed.findAll", query = "SELECT b FROM Breed b"),
    @NamedQuery(name = "Breed.findById", query = "SELECT b FROM Breed b WHERE b.id = :id"),
    @NamedQuery(name = "Breed.findByName", query = "SELECT b FROM Breed b WHERE b.name = :name"),
    @NamedQuery(name = "Breed.findByCreationDate", query = "SELECT b FROM Breed b WHERE b.creationDate = :creationDate"),
    @NamedQuery(name = "Breed.findByLastUpdate", query = "SELECT b FROM Breed b WHERE b.lastUpdate = :lastUpdate")})
public class Breed implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;
    @Column(name = "name")
    private String name;
    @Lob
    @Column(name = "descr")
    private String descr;
    @Basic(optional = false)
    @Column(name = "creation_date")
    @Temporal(TemporalType.TIMESTAMP)
    private Date creationDate;
    @Basic(optional = false)
    @Column(name = "last_update")
    @Temporal(TemporalType.TIMESTAMP)
    private Date lastUpdate;
    @JoinTable(name = "breed_breeder", joinColumns = {
        @JoinColumn(name = "breed_id", referencedColumnName = "id")}, inverseJoinColumns = {
        @JoinColumn(name = "breeder_id", referencedColumnName = "id")})
    @ManyToMany
    private List<Breeder> breederList;
    @OneToMany(mappedBy = "breedId")
    private List<Dog> dogList;

    public Breed() {
    }

    public Breed(Integer id) {
        this.id = id;
    }

    public Breed(Integer id, Date creationDate, Date lastUpdate) {
        this.id = id;
        this.creationDate = creationDate;
        this.lastUpdate = lastUpdate;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescr() {
        return descr;
    }

    public void setDescr(String descr) {
        this.descr = descr;
    }

    public Date getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(Date creationDate) {
        this.creationDate = creationDate;
    }

    public Date getLastUpdate() {
        return lastUpdate;
    }

    public void setLastUpdate(Date lastUpdate) {
        this.lastUpdate = lastUpdate;
    }

    public List<Breeder> getBreederList() {
        return breederList;
    }

    public void setBreederList(List<Breeder> breederList) {
        this.breederList = breederList;
    }

    public List<Dog> getDogList() {
        return dogList;
    }

    public void setDogList(List<Dog> dogList) {
        this.dogList = dogList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Breed)) {
            return false;
        }
        Breed other = (Breed) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.mycompany.myapp.Breed[ id=" + id + " ]";
    }
    
}
