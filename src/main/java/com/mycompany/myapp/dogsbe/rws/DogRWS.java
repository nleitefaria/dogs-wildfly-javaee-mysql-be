package com.mycompany.myapp.dogsbe.rws;

import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Response;

import org.apache.log4j.Logger;

import com.mycompany.myapp.dogsbe.dao.exception.IllegalOrphanException;
import com.mycompany.myapp.dogsbe.dao.exception.NonexistentEntityException;
import com.mycompany.myapp.dogsbe.domain.DogDTO;
import com.mycompany.myapp.dogsbe.service.DogService;
import com.mycompany.myapp.dogsbe.service.impl.DogServiceImpl;


@Path("/dog")
public class DogRWS
{
	final static Logger logger = Logger.getLogger(DogRWS.class);
	
	private DogService dogService;
	
	public DogRWS()
	{
		dogService = new DogServiceImpl();
	}
	
	@GET
	@Path("/{id}")
	@Produces("application/json")
	public DogDTO findOne(@PathParam("id") String id)
	{
		logger.info("@findOne");
		return dogService.findDog(id);
	}
	
	@GET
	@Path("/")
	@Produces("application/json")
	public List<DogDTO> findAll()
	{	
		return dogService.findDogEntities();		
	}
	
	@POST
	@Path("/")
	@Consumes("application/json")
	public Response create(DogDTO dogDTO) 
	{
		dogService.create(dogDTO);
		String result = "Dog created";
		return Response.status(201).entity(result).build();
	}
	
	@PUT
	@Consumes("application/json")
    public Response update(DogDTO dogDTO) 
	{
		logger.info("@edit");
		try 
		{
			dogService.edit(dogDTO);
			return Response.status(204).entity("Dog updated").build();
		} 
		catch (IllegalOrphanException e) 
		{
			return Response.status(200).entity("IllegalOrphanException").build();
		} 
		catch (NonexistentEntityException e) 
		{
			return Response.status(200).entity("NonexistentEntityException").build();
		} 
		catch (Exception e) 
		{
			return Response.status(200).entity("Exception").build();
		}    
    } 
	
	@DELETE
	@Path("/{id}")
	public Response delete(@PathParam("id") String id) 
	{
		logger.info("@delete");
		try 
		{
			dogService.delete(id);
			return Response.status(204).entity("Dog deleted").build();
		} 
		catch (NumberFormatException e) 
		{
			return Response.status(200).entity("NumberFormatException").build();
		} 
		catch (IllegalOrphanException e) 
		{
			return Response.status(200).entity("IllegalOrphanException").build();
		} 
		catch (NonexistentEntityException e) 
		{
			return Response.status(200).entity("NonexistentEntityException").build();
		}
	}

}
