package com.mycompany.myapp.dogsbe.rws;

import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Response;

import org.apache.log4j.Logger;

import com.mycompany.myapp.dogsbe.dao.exception.IllegalOrphanException;
import com.mycompany.myapp.dogsbe.dao.exception.NonexistentEntityException;
import com.mycompany.myapp.dogsbe.domain.OwnerDTO;
import com.mycompany.myapp.dogsbe.service.OwnerService;
import com.mycompany.myapp.dogsbe.service.impl.OwnerServiceImpl;

@Path("/owners")
public class OwnerRWS 
{
	final static Logger logger = Logger.getLogger(OwnerRWS.class);
	
	private OwnerService ownerService;
	
	public OwnerRWS()
	{
		ownerService = new OwnerServiceImpl();
	}
	
	@GET
	@Path("/{id}")
	@Produces("application/json")
	public OwnerDTO findOne(@PathParam("id") String id)
	{
		logger.info("@findOne");
		return ownerService.findOwner(id);
	}
	
	@GET
	@Path("/")
	@Produces("application/json")
	public List<OwnerDTO> findAll()
	{
		logger.info("@findAll");
		return ownerService.findOwnerEntities();	
	}
	
	@POST
	@Path("/")
	@Consumes("application/json")
	public Response create(OwnerDTO ownerDTO) 
	{
		logger.info("@create");
		ownerService.create(ownerDTO);
		String result = "Owner created";
		return Response.status(201).entity(result).build();
	}
	
	@PUT
	@Consumes("application/json")
    public Response update(OwnerDTO ownerDTO) 
	{		
		logger.info("@update");
		try 
		{
			ownerService.edit(ownerDTO);
			return Response.status(204).entity("Owner updated").build();
		} 
		catch (IllegalOrphanException e) 
		{
			return Response.status(200).entity("IllegalOrphanException").build();
		} 
		catch (NonexistentEntityException e) 
		{
			return Response.status(200).entity("NonexistentEntityException").build();
		} 
		catch (Exception e) 
		{
			return Response.status(200).entity("Exception").build();
		}    
    } 
	
	@DELETE
	@Path("/{id}")
	public Response delete(@PathParam("id") String id) 
	{
		logger.info("@delete");
		try 
		{
			ownerService.delete(id);
			return Response.status(204).entity("Owner deleted").build();
		} 
		catch (NumberFormatException e) 
		{
			return Response.status(200).entity("NumberFormatException").build();
		} 
		catch (IllegalOrphanException e) 
		{
			return Response.status(200).entity("IllegalOrphanException").build();
		} 
		catch (NonexistentEntityException e) 
		{
			return Response.status(200).entity("NonexistentEntityException").build();
		}
	}
	

}
