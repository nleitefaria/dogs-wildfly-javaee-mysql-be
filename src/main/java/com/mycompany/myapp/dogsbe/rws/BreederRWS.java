package com.mycompany.myapp.dogsbe.rws;

import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Response;

import org.apache.log4j.Logger;

import com.mycompany.myapp.dogsbe.dao.exception.IllegalOrphanException;
import com.mycompany.myapp.dogsbe.dao.exception.NonexistentEntityException;
import com.mycompany.myapp.dogsbe.domain.BreederDTO;
import com.mycompany.myapp.dogsbe.service.BreederService;
import com.mycompany.myapp.dogsbe.service.impl.BreederServiceImpl;

@Path("/breeder")
public class BreederRWS {
	
	final static Logger logger = Logger.getLogger(BreederRWS.class);
	
	private BreederService breederService;
	
	public BreederRWS()
	{
		breederService = new BreederServiceImpl();
	}
	
	@GET
	@Path("/{id}")
	@Produces("application/json")
	public BreederDTO findOne(@PathParam("id") String id)
	{
		logger.info("@findOne");
		return breederService.findBreeder(id);
	}
	
	@GET
	@Path("/")
	@Produces("application/json")
	public List<BreederDTO> findAll()
	{
		logger.info("@findAll");
		return breederService.findBreederEntities();		
	}
	
	@POST
	@Path("/")
	@Consumes("application/json")
	public Response create(BreederDTO breederDTO) 
	{
		logger.info("@create");
		breederService.create(breederDTO);
		String result = "Breeder created";
		return Response.status(201).entity(result).build();
	}
	
	@PUT
	@Consumes("application/json")
    public Response update(BreederDTO breederDTO) 
	{
		logger.info("@edit");
		try 
		{
			breederService.edit(breederDTO);
			return Response.status(204).entity("Breeder updated").build();
		} 
		catch (IllegalOrphanException e) 
		{
			return Response.status(200).entity("IllegalOrphanException").build();
		} 
		catch (NonexistentEntityException e) 
		{
			return Response.status(200).entity("NonexistentEntityException").build();
		} 
		catch (Exception e) 
		{
			return Response.status(200).entity("Exception").build();
		}    
    } 
	
	@DELETE
	@Path("/{id}")
	public Response delete(@PathParam("id") String id) 
	{
		logger.info("@delete");
		try 
		{
			breederService.delete(id);
			return Response.status(204).entity("Breeder deleted").build();
		} 
		catch (NumberFormatException e) 
		{
			return Response.status(200).entity("NumberFormatException").build();
		} 
		catch (IllegalOrphanException e) 
		{
			return Response.status(200).entity("IllegalOrphanException").build();
		} 
		catch (NonexistentEntityException e) 
		{
			return Response.status(200).entity("NonexistentEntityException").build();
		}
	}

}
