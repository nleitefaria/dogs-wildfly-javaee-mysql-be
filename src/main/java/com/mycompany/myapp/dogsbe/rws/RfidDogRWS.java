package com.mycompany.myapp.dogsbe.rws;

import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Response;

import org.apache.log4j.Logger;

import com.mycompany.myapp.dogsbe.domain.RfidDogDTO;
import com.mycompany.myapp.dogsbe.service.RfidDogService;
import com.mycompany.myapp.dogsbe.service.impl.RfidDogServiceImpl;

@Path("/rfiddog")
public class RfidDogRWS 
{
	final static Logger logger = Logger.getLogger(RfidDogRWS.class);
	
	private RfidDogService rfidDogService;
	
	public RfidDogRWS()
	{
		rfidDogService = new RfidDogServiceImpl();
	}
	
	@GET
	@Path("/{id}")
	@Produces("application/json")
	public RfidDogDTO findOne(@PathParam("id") String id)
	{
		logger.info("@findOne");
		return rfidDogService.findRfidDog(id);
	}
	
	@GET
	@Path("/")
	@Produces("application/json")
	public List<RfidDogDTO> findAll()
	{	
		logger.info("@findAll");
		return rfidDogService.findRfidDogEntities();	
	}
	
	@POST
	@Path("/")
	@Consumes("application/json")
	public Response create(RfidDogDTO rfidDogDTO) 
	{
		logger.info("@create");
		rfidDogService.create(rfidDogDTO);
		String result = "RfidDog created";
		return Response.status(201).entity(result).build();
	}
}
