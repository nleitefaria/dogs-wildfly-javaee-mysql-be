package com.mycompany.myapp.dogsbe.rws;

import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Response;

import org.apache.log4j.Logger;

import com.mycompany.myapp.dogsbe.dao.exception.IllegalOrphanException;
import com.mycompany.myapp.dogsbe.dao.exception.NonexistentEntityException;
import com.mycompany.myapp.dogsbe.domain.BreedDTO;
import com.mycompany.myapp.dogsbe.service.BreedService;
import com.mycompany.myapp.dogsbe.service.impl.BreedServiceImpl;

@Path("/breed")
public class BreedRWS 
{
	final static Logger logger = Logger.getLogger(BreedRWS.class);
	
	private BreedService breedService;
	
	public BreedRWS()
	{
		breedService = new BreedServiceImpl();
	}
	
	@GET
	@Path("/{id}")
	@Produces("application/json")
	public BreedDTO findOne(@PathParam("id") String id)
	{
		logger.info("@findOne");
		return breedService.findBreed(id);
	}
	
	@GET
	@Path("/")
	@Produces("application/json")
	public List<BreedDTO> findAll()
	{
		logger.info("@findAll");
		return breedService.findBreedEntities();		
	}
	
	@POST
	@Path("/")
	@Consumes("application/json")
	public Response create(BreedDTO breedDTO) 
	{
		breedService.create(breedDTO);
		String result = "Breed created";
		return Response.status(201).entity(result).build();
	}
	
	@PUT
	@Consumes("application/json")
    public Response update(BreedDTO breederDTO) 
	{
		logger.info("@edit");
		try 
		{
			breedService.edit(breederDTO);
			return Response.status(204).entity("Breed updated").build();
		} 
		catch (IllegalOrphanException e) 
		{
			return Response.status(200).entity("IllegalOrphanException").build();
		} 
		catch (NonexistentEntityException e) 
		{
			return Response.status(200).entity("NonexistentEntityException").build();
		} 
		catch (Exception e) 
		{
			return Response.status(200).entity("Exception").build();
		}    
    } 
	
	@DELETE
	@Path("/{id}")
	public Response delete(@PathParam("id") String id) 
	{
		logger.info("@delete");
		try 
		{
			breedService.delete(id);
			return Response.status(204).entity("Breed deleted").build();
		} 
		catch (NumberFormatException e) 
		{
			return Response.status(200).entity("NumberFormatException").build();
		} 
		catch (IllegalOrphanException e) 
		{
			return Response.status(200).entity("IllegalOrphanException").build();
		} 
		catch (NonexistentEntityException e) 
		{
			return Response.status(200).entity("NonexistentEntityException").build();
		}
	}


}
