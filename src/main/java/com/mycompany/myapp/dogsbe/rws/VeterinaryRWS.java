package com.mycompany.myapp.dogsbe.rws;

import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Response;

import org.apache.log4j.Logger;

import com.mycompany.myapp.dogsbe.dao.exception.IllegalOrphanException;
import com.mycompany.myapp.dogsbe.dao.exception.NonexistentEntityException;
import com.mycompany.myapp.dogsbe.domain.VeterinaryDTO;
import com.mycompany.myapp.dogsbe.service.VeterinaryService;
import com.mycompany.myapp.dogsbe.service.impl.VeterinaryServiceImpl;

@Path("/veterinary")
public class VeterinaryRWS
{
	final static Logger logger = Logger.getLogger(VeterinaryRWS.class);
	
	private VeterinaryService veterinaryService;
	
	public VeterinaryRWS()
	{
		veterinaryService = new VeterinaryServiceImpl();
	}
	
	@GET
	@Path("/{id}")
	@Produces("application/json")
	public VeterinaryDTO findOne(@PathParam("id") String id)
	{
		logger.info("@findOne");
		return veterinaryService.findVeterinary(id);
	}
	
	@GET
	@Path("/")
	@Produces("application/json")
	public List<VeterinaryDTO> findAll()
	{	
		logger.info("@findAll");
		return veterinaryService.findVeterinaryEntities();	
	}
	
	@POST
	@Path("/")
	@Consumes("application/json")
	public Response create(VeterinaryDTO veterinaryDTO) 
	{
		logger.info("@create");
		veterinaryService.create(veterinaryDTO);
		String result = "Veterinary created";
		return Response.status(201).entity(result).build();
	}
	
	@PUT
	@Consumes("application/json")
    public Response update(VeterinaryDTO veterinaryDTO) 
	{
		logger.info("@edit");
		try 
		{
			veterinaryService.edit(veterinaryDTO);
			return Response.status(204).entity("Veterinary updated").build();
		} 
		catch (IllegalOrphanException e) 
		{
			return Response.status(200).entity("IllegalOrphanException").build();
		} 
		catch (NonexistentEntityException e) 
		{
			return Response.status(200).entity("NonexistentEntityException").build();
		} 
		catch (Exception e) 
		{
			return Response.status(200).entity("Exception").build();
		}    
    } 
	
	@DELETE
	@Path("/{id}")
	public Response delete(@PathParam("id") String id) 
	{
		logger.info("@delete");
		try 
		{
			veterinaryService.delete(id);
			return Response.status(204).entity("Veterinary deleted").build();
		} 
		catch (NumberFormatException e) 
		{
			return Response.status(200).entity("NumberFormatException").build();
		} 
		catch (IllegalOrphanException e) 
		{
			return Response.status(200).entity("IllegalOrphanException").build();
		} 
		catch (NonexistentEntityException e) 
		{
			return Response.status(200).entity("NonexistentEntityException").build();
		}
	}
}
