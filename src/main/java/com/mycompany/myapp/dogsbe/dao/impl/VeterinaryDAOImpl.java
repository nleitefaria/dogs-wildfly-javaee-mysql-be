package com.mycompany.myapp.dogsbe.dao.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityNotFoundException;
import javax.persistence.Query;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

import com.mycompany.myapp.dogsbe.dao.VeterinaryDAO;
import com.mycompany.myapp.dogsbe.dao.exception.IllegalOrphanException;
import com.mycompany.myapp.dogsbe.dao.exception.NonexistentEntityException;
import com.mycompany.myapp.dogsbe.entity.Dog;
import com.mycompany.myapp.dogsbe.entity.Veterinary;
import com.mycompany.myapp.dogsbe.entity.VeterinaryContact;


public class VeterinaryDAOImpl implements VeterinaryDAO, Serializable
{
	private EntityManagerFactory emf = null;

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }
    
    public VeterinaryDAOImpl(EntityManagerFactory emf) {
        this.emf = emf;
    }

	public void create(Veterinary veterinary) {
        if (veterinary.getDogList() == null) {
            veterinary.setDogList(new ArrayList<Dog>());
        }
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            VeterinaryContact veterinaryContact = veterinary.getVeterinaryContact();
            if (veterinaryContact != null) {
                veterinaryContact = em.getReference(veterinaryContact.getClass(), veterinaryContact.getVeterinaryId());
                veterinary.setVeterinaryContact(veterinaryContact);
            }
            List<Dog> attachedDogList = new ArrayList<Dog>();
            for (Dog dogListDogToAttach : veterinary.getDogList()) {
                dogListDogToAttach = em.getReference(dogListDogToAttach.getClass(), dogListDogToAttach.getId());
                attachedDogList.add(dogListDogToAttach);
            }
            veterinary.setDogList(attachedDogList);
            em.persist(veterinary);
            if (veterinaryContact != null) {
                Veterinary oldVeterinaryOfVeterinaryContact = veterinaryContact.getVeterinary();
                if (oldVeterinaryOfVeterinaryContact != null) {
                    oldVeterinaryOfVeterinaryContact.setVeterinaryContact(null);
                    oldVeterinaryOfVeterinaryContact = em.merge(oldVeterinaryOfVeterinaryContact);
                }
                veterinaryContact.setVeterinary(veterinary);
                veterinaryContact = em.merge(veterinaryContact);
            }
            for (Dog dogListDog : veterinary.getDogList()) {
                Veterinary oldVeterinaryIdOfDogListDog = dogListDog.getVeterinaryId();
                dogListDog.setVeterinaryId(veterinary);
                dogListDog = em.merge(dogListDog);
                if (oldVeterinaryIdOfDogListDog != null) {
                    oldVeterinaryIdOfDogListDog.getDogList().remove(dogListDog);
                    oldVeterinaryIdOfDogListDog = em.merge(oldVeterinaryIdOfDogListDog);
                }
            }
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(Veterinary veterinary) throws IllegalOrphanException, NonexistentEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Veterinary persistentVeterinary = em.find(Veterinary.class, veterinary.getId());
            VeterinaryContact veterinaryContactOld = persistentVeterinary.getVeterinaryContact();
            VeterinaryContact veterinaryContactNew = veterinary.getVeterinaryContact();
            List<Dog> dogListOld = persistentVeterinary.getDogList();
            List<Dog> dogListNew = veterinary.getDogList();
            List<String> illegalOrphanMessages = null;
            if (veterinaryContactOld != null && !veterinaryContactOld.equals(veterinaryContactNew)) {
                if (illegalOrphanMessages == null) {
                    illegalOrphanMessages = new ArrayList<String>();
                }
                illegalOrphanMessages.add("You must retain VeterinaryContact " + veterinaryContactOld + " since its veterinary field is not nullable.");
            }
            if (illegalOrphanMessages != null) {
                throw new IllegalOrphanException(illegalOrphanMessages);
            }
            if (veterinaryContactNew != null) {
                veterinaryContactNew = em.getReference(veterinaryContactNew.getClass(), veterinaryContactNew.getVeterinaryId());
                veterinary.setVeterinaryContact(veterinaryContactNew);
            }
            List<Dog> attachedDogListNew = new ArrayList<Dog>();
            for (Dog dogListNewDogToAttach : dogListNew) {
                dogListNewDogToAttach = em.getReference(dogListNewDogToAttach.getClass(), dogListNewDogToAttach.getId());
                attachedDogListNew.add(dogListNewDogToAttach);
            }
            dogListNew = attachedDogListNew;
            veterinary.setDogList(dogListNew);
            veterinary = em.merge(veterinary);
            if (veterinaryContactNew != null && !veterinaryContactNew.equals(veterinaryContactOld)) {
                Veterinary oldVeterinaryOfVeterinaryContact = veterinaryContactNew.getVeterinary();
                if (oldVeterinaryOfVeterinaryContact != null) {
                    oldVeterinaryOfVeterinaryContact.setVeterinaryContact(null);
                    oldVeterinaryOfVeterinaryContact = em.merge(oldVeterinaryOfVeterinaryContact);
                }
                veterinaryContactNew.setVeterinary(veterinary);
                veterinaryContactNew = em.merge(veterinaryContactNew);
            }
            for (Dog dogListOldDog : dogListOld) {
                if (!dogListNew.contains(dogListOldDog)) {
                    dogListOldDog.setVeterinaryId(null);
                    dogListOldDog = em.merge(dogListOldDog);
                }
            }
            for (Dog dogListNewDog : dogListNew) {
                if (!dogListOld.contains(dogListNewDog)) {
                    Veterinary oldVeterinaryIdOfDogListNewDog = dogListNewDog.getVeterinaryId();
                    dogListNewDog.setVeterinaryId(veterinary);
                    dogListNewDog = em.merge(dogListNewDog);
                    if (oldVeterinaryIdOfDogListNewDog != null && !oldVeterinaryIdOfDogListNewDog.equals(veterinary)) {
                        oldVeterinaryIdOfDogListNewDog.getDogList().remove(dogListNewDog);
                        oldVeterinaryIdOfDogListNewDog = em.merge(oldVeterinaryIdOfDogListNewDog);
                    }
                }
            }
            em.getTransaction().commit();
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                Integer id = veterinary.getId();
                if (findVeterinary(id) == null) {
                    throw new NonexistentEntityException("The veterinary with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(Integer id) throws IllegalOrphanException, NonexistentEntityException {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Veterinary veterinary;
            try {
                veterinary = em.getReference(Veterinary.class, id);
                veterinary.getId();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The veterinary with id " + id + " no longer exists.", enfe);
            }
            List<String> illegalOrphanMessages = null;
            VeterinaryContact veterinaryContactOrphanCheck = veterinary.getVeterinaryContact();
            if (veterinaryContactOrphanCheck != null) {
                if (illegalOrphanMessages == null) {
                    illegalOrphanMessages = new ArrayList<String>();
                }
                illegalOrphanMessages.add("This Veterinary (" + veterinary + ") cannot be destroyed since the VeterinaryContact " + veterinaryContactOrphanCheck + " in its veterinaryContact field has a non-nullable veterinary field.");
            }
            if (illegalOrphanMessages != null) {
                throw new IllegalOrphanException(illegalOrphanMessages);
            }
            List<Dog> dogList = veterinary.getDogList();
            for (Dog dogListDog : dogList) {
                dogListDog.setVeterinaryId(null);
                dogListDog = em.merge(dogListDog);
            }
            em.remove(veterinary);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<Veterinary> findVeterinaryEntities() {
        return findVeterinaryEntities(true, -1, -1);
    }

    public List<Veterinary> findVeterinaryEntities(int maxResults, int firstResult) {
        return findVeterinaryEntities(false, maxResults, firstResult);
    }

    private List<Veterinary> findVeterinaryEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            cq.select(cq.from(Veterinary.class));
            Query q = em.createQuery(cq);
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public Veterinary findVeterinary(Integer id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(Veterinary.class, id);
        } finally {
            em.close();
        }
    }

    public int getVeterinaryCount() {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            Root<Veterinary> rt = cq.from(Veterinary.class);
            cq.select(em.getCriteriaBuilder().count(rt));
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }
    
}
