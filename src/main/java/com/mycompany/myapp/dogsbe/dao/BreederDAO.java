package com.mycompany.myapp.dogsbe.dao;

import java.util.List;

import com.mycompany.myapp.dogsbe.dao.exception.NonexistentEntityException;
import com.mycompany.myapp.dogsbe.entity.Breeder;

public interface BreederDAO 
{
	void create(Breeder breeder);
	void edit(Breeder breeder) throws NonexistentEntityException, Exception;
	void destroy(Integer id) throws NonexistentEntityException;
	List<Breeder> findBreederEntities();
	List<Breeder> findBreederEntities(int maxResults, int firstResult);
	Breeder findBreeder(Integer id);
	int getBreederCount();
}
