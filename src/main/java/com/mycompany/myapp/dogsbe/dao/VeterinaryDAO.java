package com.mycompany.myapp.dogsbe.dao;

import java.util.List;

import com.mycompany.myapp.dogsbe.dao.exception.IllegalOrphanException;
import com.mycompany.myapp.dogsbe.dao.exception.NonexistentEntityException;
import com.mycompany.myapp.dogsbe.entity.Veterinary;

public interface VeterinaryDAO 
{
	void create(Veterinary veterinary);
	void edit(Veterinary veterinary) throws IllegalOrphanException, NonexistentEntityException, Exception;
	void destroy(Integer id) throws IllegalOrphanException, NonexistentEntityException;
	List<Veterinary> findVeterinaryEntities();
	List<Veterinary> findVeterinaryEntities(int maxResults, int firstResult);
	Veterinary findVeterinary(Integer id);
	int getVeterinaryCount();
}
