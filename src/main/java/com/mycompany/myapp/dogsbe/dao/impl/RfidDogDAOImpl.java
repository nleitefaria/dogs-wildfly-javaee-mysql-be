package com.mycompany.myapp.dogsbe.dao.impl;

import java.io.Serializable;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

import com.mycompany.myapp.dogsbe.dao.RfidDogDAO;
import com.mycompany.myapp.dogsbe.dao.exception.NonexistentEntityException;
import com.mycompany.myapp.dogsbe.entity.RfidDog;

import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;

public class RfidDogDAOImpl implements RfidDogDAO, Serializable 
{
	public RfidDogDAOImpl(EntityManagerFactory emf)
	{
		this.emf = emf;
	}

	private EntityManagerFactory emf = null;

	public EntityManager getEntityManager() 
	{
		return emf.createEntityManager();
	}

	public void create(RfidDog rfidDog)
	{
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            em.persist(rfidDog);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(RfidDog rfidDog) throws NonexistentEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            rfidDog = em.merge(rfidDog);
            em.getTransaction().commit();
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                Integer id = rfidDog.getDogId();
                if (findRfidDog(id) == null) {
                    throw new NonexistentEntityException("The rfidDog with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(Integer id) throws NonexistentEntityException {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            RfidDog rfidDog;
            try {
                rfidDog = em.getReference(RfidDog.class, id);
                rfidDog.getDogId();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The rfidDog with id " + id + " no longer exists.", enfe);
            }
            em.remove(rfidDog);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<RfidDog> findRfidDogEntities() {
        return findRfidDogEntities(true, -1, -1);
    }

    public List<RfidDog> findRfidDogEntities(int maxResults, int firstResult) {
        return findRfidDogEntities(false, maxResults, firstResult);
    }

    private List<RfidDog> findRfidDogEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            cq.select(cq.from(RfidDog.class));
            Query q = em.createQuery(cq);
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public RfidDog findRfidDog(Integer id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(RfidDog.class, id);
        } finally {
            em.close();
        }
    }

    public int getRfidDogCount() {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            Root<RfidDog> rt = cq.from(RfidDog.class);
            cq.select(em.getCriteriaBuilder().count(rt));
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }
    
}
