package com.mycompany.myapp.dogsbe.dao.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityNotFoundException;
import javax.persistence.Query;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

import com.mycompany.myapp.dogsbe.dao.OwnerDAO;
import com.mycompany.myapp.dogsbe.dao.exception.IllegalOrphanException;
import com.mycompany.myapp.dogsbe.dao.exception.NonexistentEntityException;
import com.mycompany.myapp.dogsbe.entity.Dog;
import com.mycompany.myapp.dogsbe.entity.Owner;
import com.mycompany.myapp.dogsbe.entity.OwnerContact;

public class OwnerDAOImpl implements OwnerDAO, Serializable
{	
	private EntityManagerFactory emf = null;

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }
    
    public OwnerDAOImpl(EntityManagerFactory emf) {
        this.emf = emf;
    }
    
    public void create(Owner owner) {
        if (owner.getDogList() == null) {
            owner.setDogList(new ArrayList<Dog>());
        }
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            OwnerContact ownerContact = owner.getOwnerContact();
            if (ownerContact != null) {
                ownerContact = em.getReference(ownerContact.getClass(), ownerContact.getOwnerId());
                owner.setOwnerContact(ownerContact);
            }
            List<Dog> attachedDogList = new ArrayList<Dog>();
            for (Dog dogListDogToAttach : owner.getDogList()) {
                dogListDogToAttach = em.getReference(dogListDogToAttach.getClass(), dogListDogToAttach.getId());
                attachedDogList.add(dogListDogToAttach);
            }
            owner.setDogList(attachedDogList);
            em.persist(owner);
            if (ownerContact != null) {
                Owner oldOwnerOfOwnerContact = ownerContact.getOwner();
                if (oldOwnerOfOwnerContact != null) {
                    oldOwnerOfOwnerContact.setOwnerContact(null);
                    oldOwnerOfOwnerContact = em.merge(oldOwnerOfOwnerContact);
                }
                ownerContact.setOwner(owner);
                ownerContact = em.merge(ownerContact);
            }
            for (Dog dogListDog : owner.getDogList()) {
                Owner oldOwnerIdOfDogListDog = dogListDog.getOwnerId();
                dogListDog.setOwnerId(owner);
                dogListDog = em.merge(dogListDog);
                if (oldOwnerIdOfDogListDog != null) {
                    oldOwnerIdOfDogListDog.getDogList().remove(dogListDog);
                    oldOwnerIdOfDogListDog = em.merge(oldOwnerIdOfDogListDog);
                }
            }
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(Owner owner) throws IllegalOrphanException, NonexistentEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Owner persistentOwner = em.find(Owner.class, owner.getId());
            OwnerContact ownerContactOld = persistentOwner.getOwnerContact();
            OwnerContact ownerContactNew = owner.getOwnerContact();
            List<Dog> dogListOld = persistentOwner.getDogList();
            List<Dog> dogListNew = owner.getDogList();
            List<String> illegalOrphanMessages = null;
            if (ownerContactOld != null && !ownerContactOld.equals(ownerContactNew)) {
                if (illegalOrphanMessages == null) {
                    illegalOrphanMessages = new ArrayList<String>();
                }
                illegalOrphanMessages.add("You must retain OwnerContact " + ownerContactOld + " since its owner field is not nullable.");
            }
            if (illegalOrphanMessages != null) {
                throw new IllegalOrphanException(illegalOrphanMessages);
            }
            if (ownerContactNew != null) {
                ownerContactNew = em.getReference(ownerContactNew.getClass(), ownerContactNew.getOwnerId());
                owner.setOwnerContact(ownerContactNew);
            }
            List<Dog> attachedDogListNew = new ArrayList<Dog>();
            for (Dog dogListNewDogToAttach : dogListNew) {
                dogListNewDogToAttach = em.getReference(dogListNewDogToAttach.getClass(), dogListNewDogToAttach.getId());
                attachedDogListNew.add(dogListNewDogToAttach);
            }
            dogListNew = attachedDogListNew;
            owner.setDogList(dogListNew);
            owner = em.merge(owner);
            if (ownerContactNew != null && !ownerContactNew.equals(ownerContactOld)) {
                Owner oldOwnerOfOwnerContact = ownerContactNew.getOwner();
                if (oldOwnerOfOwnerContact != null) {
                    oldOwnerOfOwnerContact.setOwnerContact(null);
                    oldOwnerOfOwnerContact = em.merge(oldOwnerOfOwnerContact);
                }
                ownerContactNew.setOwner(owner);
                ownerContactNew = em.merge(ownerContactNew);
            }
            for (Dog dogListOldDog : dogListOld) {
                if (!dogListNew.contains(dogListOldDog)) {
                    dogListOldDog.setOwnerId(null);
                    dogListOldDog = em.merge(dogListOldDog);
                }
            }
            for (Dog dogListNewDog : dogListNew) {
                if (!dogListOld.contains(dogListNewDog)) {
                    Owner oldOwnerIdOfDogListNewDog = dogListNewDog.getOwnerId();
                    dogListNewDog.setOwnerId(owner);
                    dogListNewDog = em.merge(dogListNewDog);
                    if (oldOwnerIdOfDogListNewDog != null && !oldOwnerIdOfDogListNewDog.equals(owner)) {
                        oldOwnerIdOfDogListNewDog.getDogList().remove(dogListNewDog);
                        oldOwnerIdOfDogListNewDog = em.merge(oldOwnerIdOfDogListNewDog);
                    }
                }
            }
            em.getTransaction().commit();
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                Integer id = owner.getId();
                if (findOwner(id) == null) {
                    throw new NonexistentEntityException("The owner with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(Integer id) throws IllegalOrphanException, NonexistentEntityException {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Owner owner;
            try {
                owner = em.getReference(Owner.class, id);
                owner.getId();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The owner with id " + id + " no longer exists.", enfe);
            }
            List<String> illegalOrphanMessages = null;
            OwnerContact ownerContactOrphanCheck = owner.getOwnerContact();
            if (ownerContactOrphanCheck != null) {
                if (illegalOrphanMessages == null) {
                    illegalOrphanMessages = new ArrayList<String>();
                }
                illegalOrphanMessages.add("This Owner (" + owner + ") cannot be destroyed since the OwnerContact " + ownerContactOrphanCheck + " in its ownerContact field has a non-nullable owner field.");
            }
            if (illegalOrphanMessages != null) {
                throw new IllegalOrphanException(illegalOrphanMessages);
            }
            List<Dog> dogList = owner.getDogList();
            for (Dog dogListDog : dogList) {
                dogListDog.setOwnerId(null);
                dogListDog = em.merge(dogListDog);
            }
            em.remove(owner);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<Owner> findOwnerEntities() {
        return findOwnerEntities(true, -1, -1);
    }

    public List<Owner> findOwnerEntities(int maxResults, int firstResult) {
        return findOwnerEntities(false, maxResults, firstResult);
    }

    private List<Owner> findOwnerEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            cq.select(cq.from(Owner.class));
            Query q = em.createQuery(cq);
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public Owner findOwner(Integer id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(Owner.class, id);
        } finally {
            em.close();
        }
    }

    public int getOwnerCount() {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            Root<Owner> rt = cq.from(Owner.class);
            cq.select(em.getCriteriaBuilder().count(rt));
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }
    
}
