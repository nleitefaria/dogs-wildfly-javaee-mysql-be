package com.mycompany.myapp.dogsbe.dao;

import java.util.List;

import com.mycompany.myapp.dogsbe.dao.exception.NonexistentEntityException;
import com.mycompany.myapp.dogsbe.entity.Breed;

public interface BreedDAO 
{
	void create(Breed breed);
	void edit(Breed breed) throws NonexistentEntityException, Exception;
	void destroy(Integer id) throws NonexistentEntityException;
	List<Breed> findBreedEntities();
	List<Breed> findBreedEntities(int maxResults, int firstResult);
	Breed findBreed(Integer id);
	int getBreedCount();
}
