package com.mycompany.myapp.dogsbe.dao.impl;

import java.io.Serializable;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityNotFoundException;
import javax.persistence.Query;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

import com.mycompany.myapp.dogsbe.dao.exception.NonexistentEntityException;
import com.mycompany.myapp.dogsbe.entity.Breed;
import com.mycompany.myapp.dogsbe.entity.Dog;
import com.mycompany.myapp.dogsbe.entity.Owner;
import com.mycompany.myapp.dogsbe.entity.Veterinary;

public class DogDAOImpl implements Serializable
{	
    private EntityManagerFactory emf = null;

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }
    
    public DogDAOImpl(EntityManagerFactory emf) {
        this.emf = emf;
    }

    public void create(Dog dog) {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Breed breedId = dog.getBreedId();
            if (breedId != null) {
                breedId = em.getReference(breedId.getClass(), breedId.getId());
                dog.setBreedId(breedId);
            }
            Owner ownerId = dog.getOwnerId();
            if (ownerId != null) {
                ownerId = em.getReference(ownerId.getClass(), ownerId.getId());
                dog.setOwnerId(ownerId);
            }
            Veterinary veterinaryId = dog.getVeterinaryId();
            if (veterinaryId != null) {
                veterinaryId = em.getReference(veterinaryId.getClass(), veterinaryId.getId());
                dog.setVeterinaryId(veterinaryId);
            }
            em.persist(dog);
            if (breedId != null) {
                breedId.getDogList().add(dog);
                breedId = em.merge(breedId);
            }
            if (ownerId != null) {
                ownerId.getDogList().add(dog);
                ownerId = em.merge(ownerId);
            }
            if (veterinaryId != null) {
                veterinaryId.getDogList().add(dog);
                veterinaryId = em.merge(veterinaryId);
            }
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(Dog dog) throws NonexistentEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Dog persistentDog = em.find(Dog.class, dog.getId());
            Breed breedIdOld = persistentDog.getBreedId();
            Breed breedIdNew = dog.getBreedId();
            Owner ownerIdOld = persistentDog.getOwnerId();
            Owner ownerIdNew = dog.getOwnerId();
            Veterinary veterinaryIdOld = persistentDog.getVeterinaryId();
            Veterinary veterinaryIdNew = dog.getVeterinaryId();
            if (breedIdNew != null) {
                breedIdNew = em.getReference(breedIdNew.getClass(), breedIdNew.getId());
                dog.setBreedId(breedIdNew);
            }
            if (ownerIdNew != null) {
                ownerIdNew = em.getReference(ownerIdNew.getClass(), ownerIdNew.getId());
                dog.setOwnerId(ownerIdNew);
            }
            if (veterinaryIdNew != null) {
                veterinaryIdNew = em.getReference(veterinaryIdNew.getClass(), veterinaryIdNew.getId());
                dog.setVeterinaryId(veterinaryIdNew);
            }
            dog = em.merge(dog);
            if (breedIdOld != null && !breedIdOld.equals(breedIdNew)) {
                breedIdOld.getDogList().remove(dog);
                breedIdOld = em.merge(breedIdOld);
            }
            if (breedIdNew != null && !breedIdNew.equals(breedIdOld)) {
                breedIdNew.getDogList().add(dog);
                breedIdNew = em.merge(breedIdNew);
            }
            if (ownerIdOld != null && !ownerIdOld.equals(ownerIdNew)) {
                ownerIdOld.getDogList().remove(dog);
                ownerIdOld = em.merge(ownerIdOld);
            }
            if (ownerIdNew != null && !ownerIdNew.equals(ownerIdOld)) {
                ownerIdNew.getDogList().add(dog);
                ownerIdNew = em.merge(ownerIdNew);
            }
            if (veterinaryIdOld != null && !veterinaryIdOld.equals(veterinaryIdNew)) {
                veterinaryIdOld.getDogList().remove(dog);
                veterinaryIdOld = em.merge(veterinaryIdOld);
            }
            if (veterinaryIdNew != null && !veterinaryIdNew.equals(veterinaryIdOld)) {
                veterinaryIdNew.getDogList().add(dog);
                veterinaryIdNew = em.merge(veterinaryIdNew);
            }
            em.getTransaction().commit();
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                Integer id = dog.getId();
                if (findDog(id) == null) {
                    throw new NonexistentEntityException("The dog with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(Integer id) throws NonexistentEntityException {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Dog dog;
            try {
                dog = em.getReference(Dog.class, id);
                dog.getId();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The dog with id " + id + " no longer exists.", enfe);
            }
            Breed breedId = dog.getBreedId();
            if (breedId != null) {
                breedId.getDogList().remove(dog);
                breedId = em.merge(breedId);
            }
            Owner ownerId = dog.getOwnerId();
            if (ownerId != null) {
                ownerId.getDogList().remove(dog);
                ownerId = em.merge(ownerId);
            }
            Veterinary veterinaryId = dog.getVeterinaryId();
            if (veterinaryId != null) {
                veterinaryId.getDogList().remove(dog);
                veterinaryId = em.merge(veterinaryId);
            }
            em.remove(dog);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<Dog> findDogEntities() {
        return findDogEntities(true, -1, -1);
    }

    public List<Dog> findDogEntities(int maxResults, int firstResult) {
        return findDogEntities(false, maxResults, firstResult);
    }

    private List<Dog> findDogEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            cq.select(cq.from(Dog.class));
            Query q = em.createQuery(cq);
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public Dog findDog(Integer id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(Dog.class, id);
        } finally {
            em.close();
        }
    }

    public int getDogCount() {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            Root<Dog> rt = cq.from(Dog.class);
            cq.select(em.getCriteriaBuilder().count(rt));
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }
    
}
