package com.mycompany.myapp.dogsbe.dao;

import java.util.List;

import com.mycompany.myapp.dogsbe.dao.exception.NonexistentEntityException;
import com.mycompany.myapp.dogsbe.entity.RfidDog;

public interface RfidDogDAO
{	
	void create(RfidDog rfidDog);
	void edit(RfidDog rfidDog) throws NonexistentEntityException, Exception;
	void destroy(Integer id) throws NonexistentEntityException;
	List<RfidDog> findRfidDogEntities();
	List<RfidDog> findRfidDogEntities(int maxResults, int firstResult);
	RfidDog findRfidDog(Integer id);
	int getRfidDogCount();
}
