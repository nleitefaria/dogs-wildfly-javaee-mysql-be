package com.mycompany.myapp.dogsbe.dao;

import java.util.List;

import com.mycompany.myapp.dogsbe.dao.exception.IllegalOrphanException;
import com.mycompany.myapp.dogsbe.dao.exception.NonexistentEntityException;
import com.mycompany.myapp.dogsbe.entity.Owner;

public interface OwnerDAO
{
	void create(Owner owner);
	void edit(Owner owner) throws IllegalOrphanException, NonexistentEntityException, Exception;
	void destroy(Integer id) throws IllegalOrphanException, NonexistentEntityException;
	List<Owner> findOwnerEntities();
	List<Owner> findOwnerEntities(int maxResults, int firstResult);
	Owner findOwner(Integer id);
	int getOwnerCount();
}
