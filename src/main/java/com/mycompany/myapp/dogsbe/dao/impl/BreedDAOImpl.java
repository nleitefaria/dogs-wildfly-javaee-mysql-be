package com.mycompany.myapp.dogsbe.dao.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityNotFoundException;
import javax.persistence.Query;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

import com.mycompany.myapp.dogsbe.dao.BreedDAO;
import com.mycompany.myapp.dogsbe.dao.exception.NonexistentEntityException;
import com.mycompany.myapp.dogsbe.entity.Breed;
import com.mycompany.myapp.dogsbe.entity.Breeder;
import com.mycompany.myapp.dogsbe.entity.Dog;

public class BreedDAOImpl implements BreedDAO, Serializable
{	
    private EntityManagerFactory emf = null;

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }
    
    public BreedDAOImpl(EntityManagerFactory emf) {
        this.emf = emf;
    }

    public void create(Breed breed) {
        if (breed.getBreederList() == null) {
            breed.setBreederList(new ArrayList<Breeder>());
        }
        if (breed.getDogList() == null) {
            breed.setDogList(new ArrayList<Dog>());
        }
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            List<Breeder> attachedBreederList = new ArrayList<Breeder>();
            for (Breeder breederListBreederToAttach : breed.getBreederList()) {
                breederListBreederToAttach = em.getReference(breederListBreederToAttach.getClass(), breederListBreederToAttach.getId());
                attachedBreederList.add(breederListBreederToAttach);
            }
            breed.setBreederList(attachedBreederList);
            List<Dog> attachedDogList = new ArrayList<Dog>();
            for (Dog dogListDogToAttach : breed.getDogList()) {
                dogListDogToAttach = em.getReference(dogListDogToAttach.getClass(), dogListDogToAttach.getId());
                attachedDogList.add(dogListDogToAttach);
            }
            breed.setDogList(attachedDogList);
            em.persist(breed);
            for (Breeder breederListBreeder : breed.getBreederList()) {
                breederListBreeder.getBreedList().add(breed);
                breederListBreeder = em.merge(breederListBreeder);
            }
            for (Dog dogListDog : breed.getDogList()) {
                Breed oldBreedIdOfDogListDog = dogListDog.getBreedId();
                dogListDog.setBreedId(breed);
                dogListDog = em.merge(dogListDog);
                if (oldBreedIdOfDogListDog != null) {
                    oldBreedIdOfDogListDog.getDogList().remove(dogListDog);
                    oldBreedIdOfDogListDog = em.merge(oldBreedIdOfDogListDog);
                }
            }
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(Breed breed) throws NonexistentEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Breed persistentBreed = em.find(Breed.class, breed.getId());
            List<Breeder> breederListOld = persistentBreed.getBreederList();
            List<Breeder> breederListNew = breed.getBreederList();
            List<Dog> dogListOld = persistentBreed.getDogList();
            List<Dog> dogListNew = breed.getDogList();
            List<Breeder> attachedBreederListNew = new ArrayList<Breeder>();
            for (Breeder breederListNewBreederToAttach : breederListNew) {
                breederListNewBreederToAttach = em.getReference(breederListNewBreederToAttach.getClass(), breederListNewBreederToAttach.getId());
                attachedBreederListNew.add(breederListNewBreederToAttach);
            }
            breederListNew = attachedBreederListNew;
            breed.setBreederList(breederListNew);
            List<Dog> attachedDogListNew = new ArrayList<Dog>();
            for (Dog dogListNewDogToAttach : dogListNew) {
                dogListNewDogToAttach = em.getReference(dogListNewDogToAttach.getClass(), dogListNewDogToAttach.getId());
                attachedDogListNew.add(dogListNewDogToAttach);
            }
            dogListNew = attachedDogListNew;
            breed.setDogList(dogListNew);
            breed = em.merge(breed);
            for (Breeder breederListOldBreeder : breederListOld) {
                if (!breederListNew.contains(breederListOldBreeder)) {
                    breederListOldBreeder.getBreedList().remove(breed);
                    breederListOldBreeder = em.merge(breederListOldBreeder);
                }
            }
            for (Breeder breederListNewBreeder : breederListNew) {
                if (!breederListOld.contains(breederListNewBreeder)) {
                    breederListNewBreeder.getBreedList().add(breed);
                    breederListNewBreeder = em.merge(breederListNewBreeder);
                }
            }
            for (Dog dogListOldDog : dogListOld) {
                if (!dogListNew.contains(dogListOldDog)) {
                    dogListOldDog.setBreedId(null);
                    dogListOldDog = em.merge(dogListOldDog);
                }
            }
            for (Dog dogListNewDog : dogListNew) {
                if (!dogListOld.contains(dogListNewDog)) {
                    Breed oldBreedIdOfDogListNewDog = dogListNewDog.getBreedId();
                    dogListNewDog.setBreedId(breed);
                    dogListNewDog = em.merge(dogListNewDog);
                    if (oldBreedIdOfDogListNewDog != null && !oldBreedIdOfDogListNewDog.equals(breed)) {
                        oldBreedIdOfDogListNewDog.getDogList().remove(dogListNewDog);
                        oldBreedIdOfDogListNewDog = em.merge(oldBreedIdOfDogListNewDog);
                    }
                }
            }
            em.getTransaction().commit();
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                Integer id = breed.getId();
                if (findBreed(id) == null) {
                    throw new NonexistentEntityException("The breed with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(Integer id) throws NonexistentEntityException {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Breed breed;
            try {
                breed = em.getReference(Breed.class, id);
                breed.getId();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The breed with id " + id + " no longer exists.", enfe);
            }
            List<Breeder> breederList = breed.getBreederList();
            for (Breeder breederListBreeder : breederList) {
                breederListBreeder.getBreedList().remove(breed);
                breederListBreeder = em.merge(breederListBreeder);
            }
            List<Dog> dogList = breed.getDogList();
            for (Dog dogListDog : dogList) {
                dogListDog.setBreedId(null);
                dogListDog = em.merge(dogListDog);
            }
            em.remove(breed);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<Breed> findBreedEntities() {
        return findBreedEntities(true, -1, -1);
    }

    public List<Breed> findBreedEntities(int maxResults, int firstResult) {
        return findBreedEntities(false, maxResults, firstResult);
    }

    private List<Breed> findBreedEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            cq.select(cq.from(Breed.class));
            Query q = em.createQuery(cq);
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public Breed findBreed(Integer id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(Breed.class, id);
        } finally {
            em.close();
        }
    }

    public int getBreedCount() {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            Root<Breed> rt = cq.from(Breed.class);
            cq.select(em.getCriteriaBuilder().count(rt));
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }
    
}
