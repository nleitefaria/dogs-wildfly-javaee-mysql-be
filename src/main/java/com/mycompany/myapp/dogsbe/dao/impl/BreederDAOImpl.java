package com.mycompany.myapp.dogsbe.dao.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityNotFoundException;
import javax.persistence.Query;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

import com.mycompany.myapp.dogsbe.dao.BreederDAO;
import com.mycompany.myapp.dogsbe.dao.exception.NonexistentEntityException;
import com.mycompany.myapp.dogsbe.entity.Breed;
import com.mycompany.myapp.dogsbe.entity.Breeder;

public class BreederDAOImpl implements BreederDAO, Serializable 
{
	private EntityManagerFactory emf = null;

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }
    
    public BreederDAOImpl(EntityManagerFactory emf) {
        this.emf = emf;
    }
    
    public void create(Breeder breeder) {
        if (breeder.getBreedList() == null) {
            breeder.setBreedList(new ArrayList<Breed>());
        }
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            List<Breed> attachedBreedList = new ArrayList<Breed>();
            for (Breed breedListBreedToAttach : breeder.getBreedList()) {
                breedListBreedToAttach = em.getReference(breedListBreedToAttach.getClass(), breedListBreedToAttach.getId());
                attachedBreedList.add(breedListBreedToAttach);
            }
            breeder.setBreedList(attachedBreedList);
            em.persist(breeder);
            for (Breed breedListBreed : breeder.getBreedList()) {
                breedListBreed.getBreederList().add(breeder);
                breedListBreed = em.merge(breedListBreed);
            }
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(Breeder breeder) throws NonexistentEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Breeder persistentBreeder = em.find(Breeder.class, breeder.getId());
            List<Breed> breedListOld = persistentBreeder.getBreedList();
            List<Breed> breedListNew = breeder.getBreedList();
            List<Breed> attachedBreedListNew = new ArrayList<Breed>();
            for (Breed breedListNewBreedToAttach : breedListNew) {
                breedListNewBreedToAttach = em.getReference(breedListNewBreedToAttach.getClass(), breedListNewBreedToAttach.getId());
                attachedBreedListNew.add(breedListNewBreedToAttach);
            }
            breedListNew = attachedBreedListNew;
            breeder.setBreedList(breedListNew);
            breeder = em.merge(breeder);
            for (Breed breedListOldBreed : breedListOld) {
                if (!breedListNew.contains(breedListOldBreed)) {
                    breedListOldBreed.getBreederList().remove(breeder);
                    breedListOldBreed = em.merge(breedListOldBreed);
                }
            }
            for (Breed breedListNewBreed : breedListNew) {
                if (!breedListOld.contains(breedListNewBreed)) {
                    breedListNewBreed.getBreederList().add(breeder);
                    breedListNewBreed = em.merge(breedListNewBreed);
                }
            }
            em.getTransaction().commit();
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                Integer id = breeder.getId();
                if (findBreeder(id) == null) {
                    throw new NonexistentEntityException("The breeder with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(Integer id) throws NonexistentEntityException {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Breeder breeder;
            try {
                breeder = em.getReference(Breeder.class, id);
                breeder.getId();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The breeder with id " + id + " no longer exists.", enfe);
            }
            List<Breed> breedList = breeder.getBreedList();
            for (Breed breedListBreed : breedList) {
                breedListBreed.getBreederList().remove(breeder);
                breedListBreed = em.merge(breedListBreed);
            }
            em.remove(breeder);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<Breeder> findBreederEntities() {
        return findBreederEntities(true, -1, -1);
    }

    public List<Breeder> findBreederEntities(int maxResults, int firstResult) {
        return findBreederEntities(false, maxResults, firstResult);
    }

    private List<Breeder> findBreederEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            cq.select(cq.from(Breeder.class));
            Query q = em.createQuery(cq);
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public Breeder findBreeder(Integer id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(Breeder.class, id);
        } finally {
            em.close();
        }
    }

    public int getBreederCount() {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            Root<Breeder> rt = cq.from(Breeder.class);
            cq.select(em.getCriteriaBuilder().count(rt));
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }


}
